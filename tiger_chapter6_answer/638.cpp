//6.3.8. 输入一行字符，统计其中单词的个数。各单词之间用空格分隔，空格数可以是多个。
#include <stdio.h>
int main(void)
{
	char c;
	int count, word;
	c = getchar();
	count = word = 0;
	while (c != '\n') {
		if (c == ' ')
			word = 0;
		else if (word == 0) {
			count++;
			word = 1;
		}
		c = getchar();
	}
	printf("count = %d\n", count);
	return 0;
}
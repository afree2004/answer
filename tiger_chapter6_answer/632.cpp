// 6.3.2. 输入两个正整数a和n,求a+aa+aaa+aa…a(n个a之和)。要求定义并调用函数fn(a,n),
//它的功能是返回aa…a(n个a)。例如：fn(3,2)的返回值是33。
#include <stdio.h>
#include <math.h>
int fn(int a, int n);
int main()
{
	int a = 0, n = 0, sum = 0;
	scanf("%d", &a);//scanf()里不能有换行符 
	scanf("%d", &n);
	for (int j = n; j>0; j--)
	{
		sum = sum + fn(a, j);
	}
	printf("Sum=%d", sum);
	return 0;
}

int fn(int a, int n)
{
	int result = 0;
	for (int i = 0; i<n; i++)
	{
		result = result + a*pow(10, i);//pow(2,3)表示2的3次 	
	}
	return result;
}
//6.3.7 程序模拟简单运算器的工作：输入一个算式（没有空格），遇等号"="说明输入结束，输
//出结果。假设计算器只能进行加、减、乘、除运算，运算数和结果都是整数，4 种运算符的
//优先级相同，按从左到右的顺序计算。例如，输入“1 + 2 * 10 - 10 / 2 = ”后，输出 10。
#include <stdio.h>
int main(void)
{
	char op;
	int operand1, operand2, res;
	scanf("%d", &operand1);
	op = getchar();
	while (op != '=') {
		scanf("%d", &operand2);
		switch (op) {
		case '+': res = operand1 + operand2; break;
		case '-': res = operand1 - operand2; break;
		case '*': res = operand1 * operand2; break;
		case '/': res = operand1 / operand2; break;
		default: res = 0;
		}
		operand1 = res;
		op = getchar();
	}
	printf("%d\n", res);
	return 0;
}
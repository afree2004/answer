//6.3.6 使用函数输出一个整数的逆序数（20 分）
#include <stdio.h>

int reverse(int number);
int main()
{
	int n;

	scanf("%d", &n);
	printf("%d\n", reverse(n));

	return 0;
}

int reverse(int number) {
	int symbol = 1, x, sum = 0;

	if (number<0) {
		number = -number;
		symbol = -1;
	}

	while (number>0) {
		x = number % 10;
		sum = sum * 10 + x;
		number /= 10;
	}
	sum *= symbol;

	return sum;
}
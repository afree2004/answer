//6.3.5. 验证哥德巴赫猜想：任何一个不小于 6 的偶数均可表示为两个奇素数之和。例如 6 = 3 + 3，
//8 = 3 + 5，…，18 = 5 + 13。将 6～100 之间的偶数都表示成两个素数之和，打印时一行打印 5 组。
#include "stdio.h"
#include "math.h"
int main(void)
{
	int count, i, number;
	int prime(int m);
	count = 0;
	for (number = 6; number <= 100; number = number + 2) {
		for (i = 3; i <= number / 2; i = i + 2)
			if (prime(i) && prime(number - i)) {
				printf("%d=%d+%d ", number, i, number - i);
				count++;
				if (count % 5 == 0) printf("\n");
				break;
			}
	}
	return 0;
}
int prime(int m)
{
	int k, i;
	if (m == 1) return 0;
	k = sqrt(m);
	for (i = 2; i <= k; i++)
		if (m % i == 0) return 0;
	return 1;
}
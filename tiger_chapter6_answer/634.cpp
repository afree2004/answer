//6.3.4. 使用函数输出指定范围内的Fibonacci数
#include<stdio.h>
int fib(int n);
int main(int argc, char const *argv[])
{
	int m, n;
	printf("请输入两个正整数m和n:");
	while (1)
	{
		scanf("%d%d", &m, &n);
		if (m >= 1 && m <= n&&n <= 10000)
			break;
	}
	printf("%d和%d之间的所有的Fibonacci数:\n", m, n);
	int i = 1, count = 0;
	while (fib(i)<m)
		i++;
	while (fib(i) <= n)
	{
		printf("%5d", fib(i));
		count++;
		i++;
		if (count % 5 == 0)
			printf("\n");
	}


	return 0;
}

int fib(int n)
{
	int x1 = 0, x2 = 1;
	int x;
	if (n == 1)
		x = 1;
	else
	{
		while (n>1)
		{
			x = x1 + x2;
			x1 = x2;
			x2 = x;
			n--;
		}
	}

	return x;
}
//6.3.1. 输入一行字符，统计出其中的英文字母、空格、数字和其他字符的个数。
#include <stdio.h>
int main(void)
{
	char c;
	int blank, digit, letter, other;
	c = getchar();
	blank = digit = letter = other = 0;
	while (c != '\n') {
		if (c >= 'a' && c <= 'z' || c >= 'A' && c <= 'Z')
			letter++;
		else if (c >= '0' && c <= '9')
			digit++;
		else if (c == ' ')
			blank++;
		else
			other++;
		c = getchar();
	}
	printf("letter = %d, blank = %d, digit = %d, other = %d\n", letter, blank, digit, other);
	return 0;
}
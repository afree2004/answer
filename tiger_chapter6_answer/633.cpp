//6.3.3. 使用函数输出指定范围内的完数
#include <stdio.h>
int factorsum(int n);
int main(int argc, char const *argv[])
{
	int m, n;
	printf("输入两个正整数m和n:");
	while (1)
	{
		scanf("%d%d", &m, &n);
		if (m >= 1 && m <= n&&n <= 1000)
			break;
		printf("请重新输入:");
	}

	printf("%d和%d之间的完数为:\n", m, n);
	int i;
	for (i = m; i <= n; i++)
	{
		if (i == factorsum(i))
			printf("%d\n", i);
	}

	return 0;
}

int factorsum(int n)
{
	int i;
	int sum = 0;
	for (i = 1; i <= n / 2; i++)
	{
		if (n%i == 0)
			sum += i;
	}

	return sum;
}